package numbers;

public class NumberGame {
    public static void main(String[] args) {
        Activities player = new Activities(10);
        player.printSquaresUptoLimit();
        player.printCubesUptoLimit();
    }

}

package numbers;

public class Activities {
    private int limit;
    public Activities(int limit) {
        this.limit = limit;
    }

    public void printSquaresUptoLimit() {
        int i = 1;
        while (i*i < limit){
            System.out.println(i*i+" ");
            i++;
        }
    }

    public void printCubesUptoLimit() {
        int i = 1;
        while (i * i * i < limit){
            System.out.println(i*i*i+" ");
            i++;
        }
    }
}
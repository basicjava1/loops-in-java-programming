package cube;

import java.util.Scanner;

public class Cube {
    public static void main(String[] args) {
       Scanner sc = new Scanner(System.in);
       int number = -1;

       do{
           if(number != -1)
               System.out.println("Cube of number is: "+(number * number *number));
           System.out.println("Enter a number: ");
           number = sc.nextInt();
       }
       while (number >= 0);
       System.out.println("Number is negative");
    }
}

package loops;

public class Loops {
    private int number;

    public Loops(int number) {
        this.number = number;
    }

    public boolean isPrime(){
        for(int i = 2; i <= number - 1; i++){
            if(number % 2 == 0)
                return false;
        }

        return true;
    }
}
